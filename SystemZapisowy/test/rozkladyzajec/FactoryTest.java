/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package rozkladyzajec;

import System_Zapisowy.GrupaZajeciowa;
import Warstwa_Prezentacji.Factory;
import java.util.IllegalFormatCodePointException;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.Rule;
import org.junit.experimental.categories.Category;
import org.junit.rules.ExpectedException;

/**
 *
 * @author lebrenzi
 */
@Category({TestControl.class, TestEntity.class})
public class FactoryTest {
    Dane dane;
    
    @Rule
    public ExpectedException exception = ExpectedException.none();

    public FactoryTest() {

    }
   
    
    @Before
    public void setUp() {
       dane = new Dane();
    }
    

    @Test
    public void testZrobGrupeZajeciowa() {
        System.out.println("zrobGrupeZajeciowa");
        Factory instance = new Factory();
        for(int i = 0; i<8; i++)
        {
            GrupaZajeciowa result = instance.zrobGrupeZajeciowa(new String[]
            {dane.daneGrup[i][0],dane.daneGrup[i][1],dane.daneGrup[i][2],dane.daneGrup[i][3],dane.daneGrup[i][4],dane.daneGrup[i][5]},
                    dane.daneGrup[i][7], dane.daneGrup[i][6]);
            assertEquals(dane.grupy[i], result);
        }
        
        exception.expect(IllegalFormatCodePointException.class);
        //exception.expectMessage("CodePoint = 0x0");
        instance.zrobGrupeZajeciowa(new String[]
            {dane.daneGrup[8][0],dane.daneGrup[8][1],dane.daneGrup[8][2],dane.daneGrup[8][3],dane.daneGrup[8][4],dane.daneGrup[8][5]},
                    dane.daneGrup[8][7], dane.daneGrup[8][6]);
        
    }
}
