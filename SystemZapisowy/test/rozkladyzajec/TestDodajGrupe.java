/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package rozkladyzajec;

import System_Zapisowy.GrupaZajeciowa;
import System_Zapisowy.RozkladZajec;
import System_Zapisowy.SystemZapisowy;
import java.util.Arrays;
import java.util.Collection;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.FixMethodOrder;
import org.junit.experimental.categories.Category;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

@Category({TestEntity.class})
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
@RunWith(Parameterized.class)
public class TestDodajGrupe {
    static Dane dane;
    static SystemZapisowy systemy[];
    public TestDodajGrupe() {
    }
    
    @Parameterized.Parameter
        public GrupaZajeciowa[][] danegrup;
    @Parameterized.Parameters
        public static Collection<Object[][][]> data(){
            Object[][][][] data1;
            data1 = new GrupaZajeciowa[][][][]{{   
                {
                    {
                        new GrupaZajeciowa(new String[]{"Pon","TP","17:15","lab"}, 3, 123, new String[]{"Bob", "Culling"}),
                        new GrupaZajeciowa(new String[]{"Pon","TN","17:15","lab"}, 4, 124, new String[]{"Bob", "Culling"}),
                        new GrupaZajeciowa(new String[]{"Wt","TP","17:15","lab"}, 5, 125, new String[]{"Bob", "Culling"}),
                        new GrupaZajeciowa(new String[]{"Pon","TP","19:15","w"}, 2, 126, new String[]{"Jack", "Culling"}),
                    },
                    {
                        new GrupaZajeciowa(new String[]{"Pt","TP","17:15","pr"}, 4, 127, new String[]{"Killian", "Murphy"}),
                        new GrupaZajeciowa(new String[]{"Pon","TP","17:15","lab"}, 6, 128, new String[]{"Andrea", "Bocceli"}),
                        new GrupaZajeciowa(new String[]{"Pt","TN","9:15","lab"}, 7, 129, new String[]{"Jason", "Statham"}),
                        new GrupaZajeciowa(new String[]{"Czw","TP","13:15","pr"}, 10, 130, new String[]{"Ryan", "Gossling"})
                    }           
                }
            }};
            return Arrays.asList(data1);
        }
    
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @BeforeClass
    public static void setUp() {
        systemy = new SystemZapisowy[2];
        systemy[0] = new SystemZapisowy();
        systemy[1] = new SystemZapisowy();
        dane = new Dane();
    }
    
    @After
    public void tearDown() {
    }

    @Test
    public void testDodajGrupe(){
        System.out.println("DodajGrupe");
        for(int i =0 ; i<2; i++)
        {
            for(int j =0 ; j<4; j++)
            {
                systemy[i].dodajGrupe(danegrup[i][j]);
                GrupaZajeciowa grupa1 = systemy[i].getRozklad().getDefinedGrupa(j);
                assertSame(grupa1, danegrup[i][j]); 
            }
                       
        }
    }
    
    @Test
    public void SzukajGrupeTest(){
        System.out.println("szukajGrupe");
        for(int i = 0; i<2;i++){
            for(int j = 0; j<4;j++)
            {   
                systemy[i].dodajGrupe(danegrup[i][j]);
                GrupaZajeciowa grupa1 = systemy[i].SzukajGrupe(danegrup[i][j].getKod());
                assertEquals(danegrup[i][j], grupa1);
            }
        }
    }
}
