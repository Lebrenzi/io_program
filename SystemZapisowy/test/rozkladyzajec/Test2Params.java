/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package rozkladyzajec;

import java.util.Arrays;
import java.util.Collection;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.experimental.categories.Category;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

@Category({TestEntity.class})
@RunWith(Parameterized.class)
public class Test2Params {
    
    Dane dane = new Dane();
    public Test2Params() {
    }
    
    @Parameterized.Parameter(value=0)
        public int numer1;
    
    @Parameterized.Parameter(value=1)
        public int numer2;
    @Parameterized.Parameters
    public static Collection<Object[]> data() 
    {
        Object[][] data1 = new Object[][]{{0,1}};
        return Arrays.asList(data1);
    }
    // {2}, {3}, {4}, {5}, {6}, {7} 
  
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
        
    }
    
    @After
    public void tearDown() {
    }
    
    @Test
    public void testObliczEcts2Params(){
        System.out.println("obliczEcts");
        int result1 = dane.rozklady[numer1].getTotalEcts();
        int result2 = dane.ectsrozkladow[numer1];
        assertEquals(result1, result2, 0);
        int result3 = dane.rozklady[numer2].getTotalEcts();
        int result4 = dane.ectsrozkladow[numer2];
        assertEquals(result3, result4, 0);
    } 
}
