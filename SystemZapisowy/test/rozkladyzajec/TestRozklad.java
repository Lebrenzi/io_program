/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package rozkladyzajec;

import java.util.Arrays;
import java.util.Collection;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.experimental.categories.Category;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

/**
 *
 * @author lebrenzi
 */
@Category({TestEntity.class})
@RunWith(Parameterized.class)
public class TestRozklad {
    
    Dane dane;
    public TestRozklad() {
    }
    
    @Parameterized.Parameter
        public int numer1;
    @Parameterized.Parameters
    public static Collection<Object[]> data() 
    {
        Object[][] data1 = new Object[][]{ {0}, {1}};
        return Arrays.asList(data1);
    }
    // {2}, {3}, {4}, {5}, {6}, {7} 
   
    
    @Before
    public void setUp() {
        dane = new Dane();
    }
    
    @After
    public void tearDown() {
    }
    
    
    @Test
    public void testEquals() {
        System.out.println("equals");
        for(int j = numer1; j<2; j++)
        {
            if(numer1==j)
                assertTrue(dane.rozklady[numer1].equals(dane.rozklady[j]));
            else
    // TODO add test methods here.
    // The methods must be annotated with annotation @Test. For example:
    //
                assertFalse(dane.rozklady[numer1].equals(dane.rozklady[j]));
        }
    }
    
    @Test
    public void testObliczEcts(){
        System.out.println("obliczEcts");
        int result1 = dane.rozklady[numer1].getTotalEcts();
        int result2 = dane.ectsrozkladow[numer1];
        assertEquals(result1, result2, 0);
    } 
}
