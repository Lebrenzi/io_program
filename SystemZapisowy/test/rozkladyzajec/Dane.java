package rozkladyzajec;


import System_Zapisowy.*;
import Warstwa_Prezentacji.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;


public class Dane {    
    public User usery[] = new User[]{
        new Student("Dzmitry", "Korsak", 239494, 5, "INF", 0, 0), 
        new Pracownik("Jan", "Gustaffson", "Administrator", "Doktor Inżynier")
    };
    public String daneGrup[][] = new String[][]{
        {"Pon","TP","17:15","lab", "Bob", "Culling","123","3"},
        {"Pon","TN","17:15","lab", "Bob", "Culling","124","4"},
        {"Wt","TP","17:15","lab", "Bob", "Culling","125","5"},
        {"Pon","TP","19:15","w", "Jack", "Culling","126","2"},
        {"Pt","TP","17:15","pr", "Killian", "Murphy","127","4"},
        {"Pon","TP","17:15","lab", "Andrea", "Bocceli","128","6"},
        {"Pt","TN","9:15","lab", "Jason", "Statham","129","7"},
        {"Czw","TP","13:15","pr", "Ryan", "Gossling","130","10"},
        {"Pon","TP","17:15","lab", "Ryan", "Gossling","401","40011"}
    };
    
    public GrupaZajeciowa[] grupy= new GrupaZajeciowa[]{
        new GrupaZajeciowa(new String[]{"Pon","TP","17:15","lab"}, 3, 123, new String[]{"Bob", "Culling"}),
        new GrupaZajeciowa(new String[]{"Pon","TN","17:15","lab"}, 4, 124, new String[]{"Bob", "Culling"}),
        new GrupaZajeciowa(new String[]{"Wt","TP","17:15","lab"}, 5, 125, new String[]{"Bob", "Culling"}),
        new GrupaZajeciowa(new String[]{"Pon","TP","19:15","w"}, 2, 126, new String[]{"Jack", "Culling"}),
        new GrupaZajeciowa(new String[]{"Pt","TP","17:15","pr"}, 4, 127, new String[]{"Killian", "Murphy"}),
        new GrupaZajeciowa(new String[]{"Pon","TP","17:15","lab"}, 6, 128, new String[]{"Andrea", "Bocceli"}),
        new GrupaZajeciowa(new String[]{"Pt","TN","9:15","lab"}, 7, 129, new String[]{"Jason", "Statham"}),
        new GrupaZajeciowa(new String[]{"Czw","TP","13:15","pr"}, 10, 130, new String[]{"Ryan", "Gossling"})
    };
    
    public GrupaZajeciowa[][] grupywrozkladzie = new GrupaZajeciowa[][]{
        {
            grupy[0], grupy[2],grupy[4],grupy[6]
        },
        {
            grupy[1], grupy[3],grupy[5],grupy[7], grupy[6]
        }
    };
    
    public int[] ectsrozkladow = new int[]{
      19,  29
    };
    
    public ArrayList<ArrayList<GrupaZajeciowa>> zajecia = new ArrayList<ArrayList<GrupaZajeciowa>>(){{
        add(new ArrayList(Arrays.asList(grupywrozkladzie[0])));
        add(new ArrayList(Arrays.asList(grupywrozkladzie[1])));
    }};
    
    public RozkladZajec rozklady[] = new RozkladZajec[]{
        new RozkladZajec(zajecia.get(0)), 
        new RozkladZajec(zajecia.get(1))
    };
    
}

