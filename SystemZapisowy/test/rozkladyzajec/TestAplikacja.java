/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package rozkladyzajec;

import System_Zapisowy.GrupaZajeciowa;
import System_Zapisowy.Student;
import System_Zapisowy.SystemZapisowy;
import Warstwa_Prezentacji.Aplikacja;
import java.util.IllegalFormatCodePointException;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.FixMethodOrder;
import org.junit.Rule;
import org.junit.experimental.categories.Category;
import org.junit.rules.ExpectedException;
import org.junit.runners.MethodSorters;

@Category({TestControl.class,TestEntity.class})
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class TestAplikacja {
    
    public TestAplikacja() {
    }
    
    static Dane dane;
    
    static Aplikacja instance1;
    static Aplikacja instance2;
    
    static SystemZapisowy test;
    
    @BeforeClass
    public static void setUp() {
        dane = new Dane();
        test = new SystemZapisowy();
        for(int i = 0; i<8; i++)
        {
            test.dodajGrupe(dane.grupy[i]);
        }
        instance1 = new Aplikacja(dane.usery[0], test);
        instance2 = new Aplikacja(dane.usery[1], test);
        
    }
    
    @Rule
     public ExpectedException exception = ExpectedException.none();
    @After
    public void tearDown() {
    }

    
    @Test
    public void testPrzDanychGrupy() {

        for(int i = 0; i<8; i++)
        {
            GrupaZajeciowa grupa1 = instance1.przegladanieDanychGrupyZajeciowej(dane.grupy[i].getKod());
            assertEquals(grupa1, dane.grupy[i]);
        }   
    }
    
    @Test
    public void testDodajStudent() {
        //Przy pierwszym dodawaniu zwracane bedzie true
        for(int i = 0; i<8; i++){
            boolean dodaj1 = instance1.ZapisDoGrupyTest(dane.grupy[i].getKod());
            assertEquals(dodaj1,true);
        }
        //Ale przy drugim juz false
        for(int i = 0; i<8; i++){
            boolean dodaj2 = instance1.ZapisDoGrupyTest(dane.grupy[i].getKod());
            assertEquals(dodaj2,false);
        }
    }
    
    public void testDodajGrupe() {
        System.out.println("dodajGrupe");
        assertEquals(instance1.dodajGrupeZajeciowa(91),false);
        assertEquals(instance2.dodajGrupeZajeciowa(91),true);
        exception.expect(IllegalFormatCodePointException.class);
        instance2.dodajGrupeZajeciowa(-123);
        
        
    }
}
