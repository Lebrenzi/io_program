/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import System_Zapisowy.GrupaZajeciowa;
import System_Zapisowy.Pracownik;
import System_Zapisowy.RozkladZajec;
import System_Zapisowy.Student;
import System_Zapisowy.SystemZapisowy;
import java.util.ArrayList;
import java.util.Arrays;
import mockit.Expectations;
import mockit.FullVerifications;
import mockit.FullVerificationsInOrder;
import mockit.Injectable;
import mockit.Mock;
import mockit.Mocked;
import mockit.StrictExpectations;
import mockit.Verifications;
import mockit.VerificationsInOrder;
import mockit.integration.junit4.JMockit;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.runner.RunWith;

@RunWith(JMockit.class)
public class SystemZapisowyTest {
    
    @Injectable
    SystemZapisowy system;
    GrupaZajeciowa grupa1;
    GrupaZajeciowa grupa2;
    Student stud1;
    Student stud2;
    
    
    @Before
    public void setUp()
    {
        grupa1 = new GrupaZajeciowa(12);
        grupa2 = new GrupaZajeciowa(13);
        stud1 = new Student("Dzmitry", "Korsak", 239494, 5, "INF_ISK", 0, 0);
        stud2 = new Student("Mikita", "Chabatarovich", 237802, 5, "INF_IMT", 0, 0);
        system.dodajGrupe(grupa1);
        system.dodajGrupe(grupa2);
        system.dodajStudent(stud1, 12);
        system.dodajStudent(stud2, 13);
    }
   
    @Test
    public void testSzukajGrupe(){
        
        System.out.println("Szukaj Grupe");
        new Expectations() {{
            
                system.SzukajGrupe(12); returns(grupa1);
                system.SzukajGrupe(13); returns(grupa2);
                system.SzukajGrupe(14); returns(null);
            
            }
        };
                assertEquals(system.SzukajGrupe(12), grupa1);
                assertEquals(system.SzukajGrupe(13), grupa2);
                assertEquals(system.SzukajGrupe(14), null);
       
        new VerificationsInOrder() {{
            
                system.SzukajGrupe(12); times =1 ;
                system.SzukajGrupe(13); times =1;
                system.SzukajGrupe(14); times =1;
            } 
        };
        
    }
    
    @Test
    public void testSprawdzZapis(){
        
        System.out.println("Sprawdz Zapis");
        new Expectations(){{
            
                system.SprawdzZapis(stud1, 12); returns(false);
                system.SprawdzZapis(stud2, 13); returns(false);
                
                system.SprawdzZapis(stud1, 13); returns(true);
                system.SprawdzZapis(stud2, 12); returns(true);
                
                system.SprawdzZapis(stud1, 14); returns(false);
                system.SprawdzZapis(stud2, 14); returns(false);
            }
        };
        
            assertEquals(system.SprawdzZapis(stud1, grupa1.getKod()), false);
            assertEquals(system.SprawdzZapis(stud2, grupa2.getKod()), false);
            
            assertEquals(system.SprawdzZapis(stud1, grupa2.getKod()), true);
            assertEquals(system.SprawdzZapis(stud2, grupa1.getKod()), true);
            
            assertEquals(system.SprawdzZapis(stud1, 14), false);
            assertEquals(system.SprawdzZapis(stud2, 14), false);
        
        new FullVerifications(){
            {
                system.SprawdzZapis(stud1, 12); maxTimes = 1;
                system.SprawdzZapis(stud2, 13); maxTimes = 1;
                
                system.SprawdzZapis(stud1, 13); maxTimes = 1;
                system.SprawdzZapis(stud2, 12); maxTimes = 1;
                
                system.SprawdzZapis(stud1, 14); maxTimes = 1;
                system.SprawdzZapis(stud2, 14); maxTimes = 1;
            }
        };
    }
            
    public SystemZapisowyTest() {
    }
 
}
