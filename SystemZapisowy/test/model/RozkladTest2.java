/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import System_Zapisowy.GrupaZajeciowa;
import System_Zapisowy.RozkladZajec;
import System_Zapisowy.SystemZapisowy;
import java.util.ArrayList;
import java.util.Arrays;
import mockit.Expectations;
import mockit.FullVerificationsInOrder;
import mockit.Mock;
import mockit.Mocked;
import mockit.integration.junit4.JMockit;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.runner.RunWith;


@RunWith(JMockit.class)
public class RozkladTest2 {
    public RozkladTest2(){};

    @Mocked
    RozkladZajec rozklad;
    
    
    @Test
    public void testEquals()
    {   
       RozkladZajec rozklad1 = new RozkladZajec();
       SystemZapisowy systems[] = {new SystemZapisowy(rozklad1), new SystemZapisowy(rozklad)};
       System.out.println("equals");
       for(int i = 0; i<1; i++)
           for(int j = i; j<2; j++)
               if(i == j )
                assertTrue(systems[i].equals(systems[i]));
               else
                assertFalse(systems[i].equals(systems[j]));
        new FullVerificationsInOrder() {
            {
                rozklad.equals(any);            maxTimes = 2;
                
            }
        };
    }
    
    @Test
        public void testObliczWartoscEcts(@Mocked final RozkladZajec rozklad1){
            
             GrupaZajeciowa[] grupy= new GrupaZajeciowa[]{
                new GrupaZajeciowa(new String[]{"Pon","TP","17:15","lab"}, 3, 123, new String[]{"Bob", "Culling"}),
                new GrupaZajeciowa(new String[]{"Pon","TN","17:15","lab"}, 4, 124, new String[]{"Bob", "Culling"}),
                new GrupaZajeciowa(new String[]{"Wt","TP","17:15","lab"}, 5, 125, new String[]{"Bob", "Culling"}),
                new GrupaZajeciowa(new String[]{"Pon","TP","19:15","w"}, 2, 126, new String[]{"Jack", "Culling"}),
                new GrupaZajeciowa(new String[]{"Pt","TP","17:15","pr"}, 4, 127, new String[]{"Killian", "Murphy"}),
                new GrupaZajeciowa(new String[]{"Pon","TP","17:15","lab"}, 6, 128, new String[]{"Andrea", "Bocceli"}),
                new GrupaZajeciowa(new String[]{"Pt","TN","9:15","lab"}, 7, 129, new String[]{"Jason", "Statham"}),
                new GrupaZajeciowa(new String[]{"Czw","TP","13:15","pr"}, 10, 130, new String[]{"Ryan", "Gossling"})
            };

            GrupaZajeciowa[][] grupywrozkladzie = new GrupaZajeciowa[][]{
                {
                    grupy[0], grupy[2],grupy[4],grupy[6]
                },
                {
                    grupy[1], grupy[3],grupy[5],grupy[7], grupy[6]
                }
            };

            int[] ectsrozkladow = new int[]{
              0,  29
            };

            ArrayList<ArrayList<GrupaZajeciowa>> zajecia = new ArrayList<ArrayList<GrupaZajeciowa>>(){{
                add(new ArrayList(Arrays.asList(grupywrozkladzie[0])));
                add(new ArrayList(Arrays.asList(grupywrozkladzie[1])));
            }};
            
            RozkladZajec rozklad2 = new RozkladZajec(zajecia.get(1));
            
            System.out.println("Oblicz ects i Szukaj pokaz grupe zajeciowa");
            new Expectations() {
                {
                    rozklad1.getTotalEcts(); result = ectsrozkladow[0];
                    
                    rozklad1.getDefinedGrupa(0);  result = null;
                    
                    rozklad2.getTotalEcts(); result = ectsrozkladow[1];
                    
                    rozklad2.getDefinedGrupa(0);   result = grupy[0];
                }
            };
            
            assertEquals(rozklad1.getTotalEcts(),ectsrozkladow[0]);
            
           

                    assertEquals(rozklad1.getDefinedGrupa(0), null);
                    
                    assertEquals(rozklad2.getTotalEcts(),ectsrozkladow[1]);
                    assertEquals(rozklad2.getDefinedGrupa(0), grupy[0]);
                
            
            new FullVerificationsInOrder() {
                {
                    rozklad1.getTotalEcts(); maxTimes =1;
                    
                    rozklad1.getDefinedGrupa(0);  maxTimes = 1;
                               
                    rozklad2.getTotalEcts(); maxTimes =1;
                    
                    rozklad2.getDefinedGrupa(0);    maxTimes = 1;
                };
            };
        }
        
       
}
