/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Suite;


import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.experimental.categories.Categories;
import org.junit.runner.RunWith;
import rozkladyzajec.TestControl;
import rozkladyzajec.*;

/**
 *
 * @author lebrenzi
 */
@Categories.SuiteClasses({FactoryTest.class, TestAplikacja.class, Test2Params.class, TestRozklad.class, TestDodajGrupe.class })
@RunWith(Categories.class)
@Categories.IncludeCategory(TestControl.class)

public class RozkladyZajecSuitControl {
}
