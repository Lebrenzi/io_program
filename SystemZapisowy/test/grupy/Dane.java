package grupy;

import System_Zapisowy.*;
import Warstwa_Prezentacji.*;

public class Dane {
    public static String daneGrup[][] = {
        {"Pon","TP","17:15","lab", "Bob", "Culling","123","3"},
        {"Pon","TN","17:15","lab", "Bob", "Culling","124","4"},
        {"Wt","TP","17:15","lab", "Bob", "Culling","125","5"},
        {"Pon","TP","19:15","w", "Jack", "Culling","126","2"},
        {"Pt","TP","17:15","pr", "Killian", "Murphy","127","4"},
        {"Pon","TP","17:15","lab", "Andrea", "Bocceli","128","6"},
        {"Pt","TN","9:15","lab", "Jason", "Statham","129","7"},
        {"Czw","TP","13:15","pr", "Ryan", "Gossling","130","10"},
        {"Pon","TP","17:15","lab", "Ryan", "Gossling","401","40011"}
    };
    
    public static GrupaZajeciowa[] grupy= {
        new GrupaZajeciowa(new String[]{"Pon","TP","17:15","lab"}, 3, 123, new String[]{"Bob", "Culling"}),
        new GrupaZajeciowa(new String[]{"Pon","TN","17:15","lab"}, 4, 124, new String[]{"Bob", "Culling"}),
        new GrupaZajeciowa(new String[]{"Wt","TP","17:15","lab"}, 5, 125, new String[]{"Bob", "Culling"}),
        new GrupaZajeciowa(new String[]{"Pon","TP","19:15","w"}, 2, 126, new String[]{"Jack", "Culling"}),
        new GrupaZajeciowa(new String[]{"Pt","TP","17:15","pr"}, 4, 127, new String[]{"Killian", "Murphy"}),
        new GrupaZajeciowa(new String[]{"Pon","TP","17:15","lab"}, 6, 128, new String[]{"Andrea", "Bocceli"}),
        new GrupaZajeciowa(new String[]{"Pt","TN","9:15","lab"}, 7, 129, new String[]{"Jason", "Statham"}),
        new GrupaZajeciowa(new String[]{"Czw","TP","13:15","pr"}, 10, 130, new String[]{"Ryan", "Gossling"})
    };
    
}
