package System_Zapisowy;

import java.util.ArrayList;
import java.util.Objects;
import mockit.Mock;
import mockit.Mocked;

public class RozkladZajec {
    private ArrayList<GrupaZajeciowa> rozklad;
    private int index;

    public int getIndex() {
        return index;
    }
    
    public RozkladZajec()
    {
        rozklad = new ArrayList<>();
    }
    
    public RozkladZajec(int index)
    {
        rozklad = new ArrayList<>();
        this.index = index;
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 61 * hash + Objects.hashCode(this.rozklad);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final RozkladZajec other = (RozkladZajec) obj;
        if (!Objects.equals(this.rozklad, other.rozklad)) {
            return false;
        }
        return true;
    }

    public RozkladZajec(ArrayList<GrupaZajeciowa> rozklad) {
        this.rozklad = rozklad;
    }

    public ArrayList<GrupaZajeciowa> getRozklad() {
        return rozklad;
    }
    
    public void dodajGrupe(GrupaZajeciowa grupa){
        rozklad.add(grupa);
    }
    
    @Mock
    public int getTotalEcts()
    {   if(rozklad == null) return 0;
        int _ects = 0;
        for(GrupaZajeciowa grupa: rozklad)
        {
           _ects+=grupa.getEcts();
        }
        return _ects;
    }
    
    @Mock
    public GrupaZajeciowa getDefinedGrupa(int definer)
    {   if(rozklad == null) return null;
        return rozklad.get(definer);
    }
}
