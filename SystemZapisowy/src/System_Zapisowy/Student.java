package System_Zapisowy;

public class Student extends User {
    private int nralbumu;
    private int semsestr_studiow;
    private String kierunek_studiow;
    private int deficyt;
    private int laczne_punkty_ects;

    public Student()
    {
        super();
        nralbumu = 123456;
        semsestr_studiow = 1;
        kierunek_studiow = "Undefined";
        deficyt = 0;
        laczne_punkty_ects = 0;
    }

    public Student(String imie, String nazwisko, int nralbumu, int semsestr_studiow, String kierunek_studiow, int deficyt, int laczne_punkty_ects) {
        super(imie, nazwisko);
        this.nralbumu = nralbumu;
        this.semsestr_studiow = semsestr_studiow;
        this.kierunek_studiow = kierunek_studiow;
        this.deficyt = deficyt;
        this.laczne_punkty_ects = laczne_punkty_ects;
    }
    
    public int getAlbum(){
        return nralbumu;
    }
}
