package System_Zapisowy;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

public class GrupaZajeciowa {

    private String dzien;
    private String parzystosc;
    private String godzina;
    private String forma_zsjec;
    private int wartosc_ects;
    private int kod_grupy;
    private String[] daneProwadzacego;
    private Map<Integer, Student> studenci;

    public GrupaZajeciowa(String[] dane ,int wartosc_ects, int kod_grupy) {
        this.dzien = dane[0];
        this.parzystosc = dane[1];
        this.godzina = dane[2];
        this.forma_zsjec = dane[3];
        this.daneProwadzacego = new String[]{dane[4], dane[5]};
        this.wartosc_ects = wartosc_ects;
        this.kod_grupy = kod_grupy;
    }

    public GrupaZajeciowa(int kod_grupy) {
        studenci = new HashMap<Integer, Student>();
        this.kod_grupy = kod_grupy;
    }

    public GrupaZajeciowa(){

    }

    public GrupaZajeciowa(String[] data, int wartosc_ects, int kod_grupy, String[] daneProwadzacego) {
        this.dzien = data[0];
        this.parzystosc = data[1];
        this.godzina = data[2];
        this.forma_zsjec = data[3];
        this.wartosc_ects = wartosc_ects;
        this.kod_grupy = kod_grupy;
        this.daneProwadzacego = daneProwadzacego;
        studenci = new HashMap<Integer, Student>();
    }

    public int getKod() {
        return kod_grupy;
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 61 * hash + Objects.hashCode(this.dzien);
        hash = 61 * hash + Objects.hashCode(this.parzystosc);
        hash = 61 * hash + Objects.hashCode(this.godzina);
        hash = 61 * hash + Objects.hashCode(this.forma_zsjec);
        hash = 61 * hash + this.wartosc_ects;
        hash = 61 * hash + this.kod_grupy;
        hash = 61 * hash + Arrays.deepHashCode(this.daneProwadzacego);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final GrupaZajeciowa other = (GrupaZajeciowa) obj;
        if (this.wartosc_ects != other.wartosc_ects) {
            return false;
        }
        if (this.kod_grupy != other.kod_grupy) {
            return false;
        }
        if (!Objects.equals(this.dzien, other.dzien)) {
            return false;
        }
        if (!Objects.equals(this.parzystosc, other.parzystosc)) {
            return false;
        }
        if (!Objects.equals(this.godzina, other.godzina)) {
            return false;
        }
        if (!Objects.equals(this.forma_zsjec, other.forma_zsjec)) {
            return false;
        }
        if (!Arrays.deepEquals(this.daneProwadzacego, other.daneProwadzacego)) {
            return false;
        }
        return true;
    }
    
    public Map<Integer, Student> getStudenci()
    {
        return studenci;
    }
    
    public void addStudenci(User student){
        Student new_student = (Student)student;
        studenci.put(new_student.getAlbum(), new_student);
    }
    
    public int getEcts(){
        return wartosc_ects;
    }
}
