package System_Zapisowy;

import java.util.ArrayList;
import java.util.Objects;
import mockit.Mock;

public class SystemZapisowy {
    RozkladZajec rozklad;

    public SystemZapisowy(RozkladZajec rozklad) {
        this.rozklad = rozklad;
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 17 * hash + Objects.hashCode(this.rozklad);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final SystemZapisowy other = (SystemZapisowy) obj;
        if (!Objects.equals(this.rozklad, other.rozklad)) {
            return false;
        }
        return true;
    }
    
    public SystemZapisowy(){
        rozklad = new RozkladZajec();
    }
    
    
    
    public boolean SprawdzZapis(User student, int kod_grupy)
    {
        GrupaZajeciowa grupa = SzukajGrupe(kod_grupy);
        Student new_student = (Student) student;
        if(grupa == null){return false;}
        else{
            if(grupa.getStudenci().containsKey(new_student.getAlbum()))
            {
                return false;
            }
            else{
                return true;
            }
        }
    }
    
    public GrupaZajeciowa SzukajGrupe(int kod_grupy)
    {
        ArrayList<GrupaZajeciowa> zajecia = rozklad.getRozklad();
        if(zajecia == null) return null;

        for(GrupaZajeciowa grupa: zajecia)
        {
            if(kod_grupy == grupa.getKod()) return grupa;
        }
        return null;
    }

    public void dodajStudent(User student, int kod_grupy)
    {
        GrupaZajeciowa grupa = SzukajGrupe(kod_grupy);
        if(grupa == null){return;}
        else{
            if(SprawdzZapis(student, kod_grupy))
            {
                grupa.addStudenci(student);
                System.out.println("Successful");
            }
            else{
                System.out.println("Failure");
                return;
            }
        }
    }
    
    public void dodajGrupe(GrupaZajeciowa grupa){
        rozklad.dodajGrupe(grupa);
    }

    public RozkladZajec getRozklad() {
        return rozklad;
    }
    

}
