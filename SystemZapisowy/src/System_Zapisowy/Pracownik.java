package System_Zapisowy;

public class Pracownik extends User{
    private String stanowisko;
    private String tytul_naukowy;

    public Pracownik() {
        super();
        stanowisko = "";
        tytul_naukowy = "";
    }

    public Pracownik(String imie, String nazwisko, String stanowisko, String tytul_naukowy) {
        super(imie, nazwisko);
        this.stanowisko = stanowisko;
        this.tytul_naukowy = tytul_naukowy;
    }
}
