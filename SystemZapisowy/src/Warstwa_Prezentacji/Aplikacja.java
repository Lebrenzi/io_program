package Warstwa_Prezentacji;

import System_Zapisowy.GrupaZajeciowa;
import System_Zapisowy.Student;
import System_Zapisowy.SystemZapisowy;
import System_Zapisowy.User;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.IllegalFormatCodePointException;


public class Aplikacja {

    private User current_user;
    private SystemZapisowy system_zapisowy;
    private int systemLevel; //0-student 1-pracownik

    public Aplikacja(User current_user, SystemZapisowy system_zapisowy) {
        this.current_user = current_user;
        if(current_user instanceof Student) systemLevel = 0;
        else systemLevel = 1;
        this.system_zapisowy = system_zapisowy;
    }

    public Aplikacja() {
        systemLevel =0;
    }

    public void signIn(){

    }

    public GrupaZajeciowa przegladanieDanychGrupyZajeciowej(int kod_grupy) throws IllegalFormatCodePointException
    {
        Factory fabryka = new Factory();
        GrupaZajeciowa grupa = fabryka.zrobGrupeZajeciowa(null, "0" , Integer.toString(kod_grupy));
        GrupaZajeciowa wynik = this.system_zapisowy.SzukajGrupe(grupa.getKod());
        if(wynik != null)
        {
            this.wyswietlDaneGrupyZajeciowej(wynik);
            return wynik;
        }
        else
        {
            this.failureKomunikat();
            return null;
        }
    }

    public boolean ZapisDoGrupy() throws IOException{
        int kod_grupy = this.WpiszKodGrupy();
        GrupaZajeciowa grupa = this.system_zapisowy.SzukajGrupe(kod_grupy);
        if(grupa == null)
        {
            failureKomunikat();
            return false;
        }
        else
        {
            if(this.system_zapisowy.SprawdzZapis(this.current_user, kod_grupy))
            {
                this.system_zapisowy.dodajStudent(this.current_user,kod_grupy);
                return true;
            }
            else
            {
                failureKomunikat();
                return false;
            }
        }  
    }
    
    public boolean ZapisDoGrupyTest(int kod_grupy){
        GrupaZajeciowa grupa = this.system_zapisowy.SzukajGrupe(kod_grupy);
        if(grupa == null)
        {
            failureKomunikat();
            return false;
        }
        else
        {
            if(this.system_zapisowy.SprawdzZapis(this.current_user, kod_grupy))
            {
                this.system_zapisowy.dodajStudent(this.current_user,kod_grupy);
                return true;
            }
            else
            {
                failureKomunikat();
                return false;
            }
        }  
    }

    public void wyswietlDaneGrupyZajeciowej(GrupaZajeciowa grupa)
    {
            System.out.println("Kod grupy: " + grupa.getKod());
    }

    public void failureKomunikat()
    {
        System.out.println("Failure");
    }

    public void successKomunikat()
    {
        System.out.println("Success");

    }

    public int WpiszKodGrupy() throws IOException
    {
        System.out.println("Wpisz kod grupy: ");
        BufferedReader buff = new BufferedReader(new InputStreamReader(System.in));
        int kod_grupy = Integer.parseInt(buff.readLine());
        return kod_grupy;
    }
    
    public boolean dodajGrupeZajeciowa(int kod_grupy) throws IllegalFormatCodePointException
    {
        if(systemLevel != 1) {
            return false;
        }
        else{
            Factory fabryka = new Factory();
            GrupaZajeciowa grupa = fabryka.zrobGrupeZajeciowa(null, "0", Integer.toString(kod_grupy));
            this.system_zapisowy.dodajGrupe(grupa);
            return true;
        }
    }

}
