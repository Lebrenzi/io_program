package Warstwa_Prezentacji;

import System_Zapisowy.GrupaZajeciowa;
import java.util.IllegalFormatCodePointException;

public class Factory {
    public Factory()
    {

    }
    public GrupaZajeciowa zrobGrupeZajeciowa(String[] daneOgolne, String ects, String kod_grupy) throws IllegalFormatCodePointException
    {   
        int ects_int = Integer.parseInt(ects);
        int kod_int = Integer.parseInt(kod_grupy);
        if(kod_int > 400 || ects_int > 400 || kod_int < 0 || ects_int < 0) throw new IllegalFormatCodePointException(0);

        if(daneOgolne == null || ects_int == 0)
        {
            return new GrupaZajeciowa(kod_int);
        }
        else return new GrupaZajeciowa(daneOgolne,ects_int,kod_int);
    }


}
