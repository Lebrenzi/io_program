/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package MainPoint;
import System_Zapisowy.*;
import Warstwa_Prezentacji.*;
import java.io.IOException;
import java.util.IllegalFormatCodePointException;
/**
 *
 * @author lebrenzi
 */
public class MainPoint {

    /**
     * @param args the command line arguments
     */
    
    public static int test(User us)
        {
            if(us instanceof Student) return 1;
            else {
                if(us instanceof Pracownik) return 2;
                else return 3;
            }
        }
    public static void main(String[] args) throws IOException, IllegalFormatCodePointException
    {
        Student test_user = new Student();
        Pracownik test_pracownik = new Pracownik();
       
        
        
        SystemZapisowy sys = new SystemZapisowy();
        //sys.dodajGrupe(new GrupaZajeciowa(12));
        //sys.dodajGrupe(new GrupaZajeciowa(13));
        //sys.dodajGrupe(new GrupaZajeciowa(14));
        Aplikacja app = new Aplikacja(test_user, sys);
        Aplikacja app1 = new Aplikacja(test_pracownik, sys);
        System.out.println(app.dodajGrupeZajeciowa(12) + " " + app1.dodajGrupeZajeciowa(12));
        app.ZapisDoGrupy();
    }
    
}
